## Reconstructor 3D

This is software under development to do 3D reconstruction in real time (or not) with mobile phone and laptop.

This intended to be Electron application which also servse
static Web App to your mobile phone in order to transmit video
frome mobile phone to Electron application for further processing.

Currently bundle adjustment algorithm still under development.

To run experiments do the following:

 - Install `ts-node`, which is `nodejs` that can build and run TypeScript on the fly https://www.npmjs.com/ts-node
 - Run `src/epip/main.ts` script using `ts-node` like so `ts-node ./src/epip/main.ts`
 - Wait a minute while `Nelder-Mead` algorithm trying to optimize bundle
 - See the following:
```
...
same motionAngleAxis = [ true, true ]
frame.structure.length = 50
restored.structure.length = 50
same structure = true
wait about a minute...
...
loss(optVal) = 51.51663970242155
loss(opt_val) = 49.88932083480519
elapsed solution = 17816
```

On the output above: 

 - `same motionAngleAxis` and `same motionAngleAxis` is sanity check done by comparison with baseline `python` implementation output
 - `optVal` hidden optimal solution for
bundle found using `Nelder-Mead` algorithm using `nodejs` package https://github.com/benfred/fmin
 - while `opt_val` is solution of the same optimization problem using `Levenberg-Marquardt` algorithm
implemented in `python` package `scipy.optimize` withing `least_squares` function https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html#scipy.optimize.least_squares
 - `17816` here is number of milliseconds elapsed during `Nelder-Mead` run
 - `loss(x)` error of solution, subject to be minimized

What this all mean:

 - `Nelder-Mead` took 17+ seconds while `Levenberg–Marquardt` (not provided here) took less than a second
 - error of `Levenberg–Marquardt` solution smaller that error of `Nelder-Mead`
 - `Levenberg–Marquardt` is better for the job than `Nelder-Mead`, higher performance 
   in several orders of magnitude is unlikely happen just because of better implementation