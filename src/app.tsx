import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as fs from 'fs'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const cv = require('../lib/opencv')
// @ts-ignore
window.cv = cv

function buildThree(o: any): any {
  let reto: any = {}
  try {
    const keys = Object.keys(o)
    keys.forEach((key: string) => {
      reto[key] = buildThree(reto[key])
    })
  } catch {
    reto = 'any'
  }
  return reto
}

console.log('cv =', cv)
console.log('cv.Mat =', cv.Mat)
console.log('Object.keys(cv) =', JSON.stringify(Object.keys(cv)))
console.log('buildThree(cv) =', buildThree(cv))

let mediaStream: MediaStream
let mediaRecorder: MediaRecorder

function render() {
  ReactDOM.render(
    <div>
      <h2>Hello from React!</h2>
      <button onClick={start}>start</button>
      <button onClick={stop}>stop</button>
      <button onClick={getImage}>get image</button>
      <br/>
      <video controls controlsList='noplay notimeline nofullscreen' style={{width: 640, height: 480}}></video>
      <canvas style={{width: 640, height: 480}}></canvas>
    </div>
    ,
    document.getElementById('react-app')
  )
}

async function start() {
  await init()
  mediaRecorder.start(0)
}

function stop() {
  mediaRecorder.stop()
  const video: HTMLVideoElement = document.querySelector('video')
  video.pause()
  mediaStream.getTracks().forEach((track: MediaStreamTrack) => track.stop())
}

async function init() {
  const devices = await navigator.mediaDevices.enumerateDevices()
  console.log('devices =', devices)
  mediaStream = await navigator.mediaDevices.getUserMedia({
    audio: true, video: true
  })
  mediaRecorder = new MediaRecorder(mediaStream)
  const video: HTMLVideoElement = document.querySelector('video')
  video.srcObject = mediaStream
  video.onloadedmetadata = function (ev: Event) {
    video.play()
  }
  const storageStream: fs.WriteStream = fs.createWriteStream('./record.mkv')
  // subInitClassic(mediaRecorder, storageStream)
  subInitPromise(mediaRecorder, storageStream)
  doGetImage()
}

function subInitClassic(recorder: MediaRecorder, storageStream: fs.WriteStream) {
  const blobs: Blob[] = []
  const blobReader: FileReader = new FileReader()
  recorder.addEventListener('dataavailable', (ev: BlobEvent) => {
    if (blobReader.readyState != 1) {
      blobReader.readAsArrayBuffer(ev.data)
    } else {
      blobs.push(ev.data)
    }
  })
  blobReader.addEventListener('load', (ev: ProgressEvent<FileReader>) => {
    const target = ev.currentTarget as FileReader
    storageStream.write(Buffer.from(target.result as ArrayBuffer))
    if (blobs.length) {
      target.readAsArrayBuffer(blobs.shift())
    }
  })
}

function subInitPromise(recorder: MediaRecorder, storageStream: fs.WriteStream) {
  let prev: Promise<ArrayBuffer> = Promise.resolve(null)
  recorder.addEventListener('dataavailable', async (ev: BlobEvent) => {
    const p: Promise<ArrayBuffer> = ev.data.arrayBuffer()
    prev = prev.then(() => p)
    const arrayBufferWaited: ArrayBuffer = await prev
    storageStream.write(Buffer.from(arrayBufferWaited))
  })
}

function getImage() {
  const video = document.querySelector('video')

  const canvas = document.querySelector('canvas')

  canvas.width = video.videoWidth
  canvas.height = video.videoHeight

  canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height)

  const dataURL = canvas.toDataURL()
  console.log('dataURL =', dataURL)
}

function doGetImage() {
  getImage()
  setTimeout(doGetImage, 0)
}

render()
