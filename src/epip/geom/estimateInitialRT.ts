import svd, { colSignToggle, rowSignToggle } from '../utils/svd'
import * as math from 'mathjs'
import * as cmp from '../utils/compare'
import { checkEstimateInitialRT } from '../data/check/check'

/**
 ESTIMATE_INITIAL_RT from the Essential Matrix, we can compute 4 initial
 guesses of the relative RT between the two cameras
 Arguments:
 E - the Essential Matrix between the two cameras
 Returns:
 RT: A 4x3x4 tensor in which the 3x4 matrix RT[i,:,:] is one of the
 four possible transformations
 */
export default function estimateInitialRT(E: number[][]): number[][][] {
  const esvd = svd(E)
  const ue = esvd.u
  const se = esvd.q
  const ve = esvd.v
  // console.log('ue =', ue)
  // console.log('se =', se)
  // console.log('ve =', ve)
  // rowSignToggle(ve, 0)
  // colSignToggle(ue, 0)
  // console.log('ve =', ve)
  // console.log('same ue =', cmp.isSameMat(ue, checkEstimateInitialRT.ue))
  // console.log('same se =', cmp.isSameVec(se, checkEstimateInitialRT.se))
  // console.log('same ve =', cmp.isSameMat(ve, checkEstimateInitialRT.ve))
  const sem = math.diag(se)
  // console.log('sem =', sem)
  const uesem = math.multiply(ue, sem)
  // console.log('uesem =', uesem)
  const res = math.multiply(uesem, ve)
  // console.log('res =', res)
  // console.log('same res =', cmp.isSameMat(res, E))
  // process.exit(0)
  const z = [
    [0, 1, 0],
    [-1, 0, 0],
    [0, 0, 0]
  ]
  const w = [
    [0, -1, 0],
    [1, 0, 0],
    [0, 0, 1]
  ]
  const q1 = math.multiply(math.multiply(ue, w), ve)
  // console.log('q1 =', q1)
  const q2 = math.multiply(math.multiply(ue, math.transpose(w)), ve)
  // console.log('q2 =', q2)
  const r1 = math.multiply(q1, math.det(q1))
  // console.log('r1 =', r1)
  const r2 = math.multiply(q2, math.det(q2))
  // console.log('r2 =', r2)
  const t1 = ue.slice().map((row: number[]) => row.slice(2, 3))
  // console.log('t1 =', t1)
  const t2 = math.multiply(t1, -1)
  // console.log('t2 =', t2)
  const r1t1 = r1.slice().map((row: number[], i) => row.slice().concat(t1[i]))
  // console.log('r1t1 =', r1t1)
  const r1t2 = r1.slice().map((row: number[], i) => row.slice().concat(t2[i]))
  // console.log('r1t2 =', r1t2)
  const r2t1 = r2.slice().map((row: number[], i) => row.slice().concat(t1[i]))
  // console.log('r2t1 =', r2t1)
  const r2t2 = r2.slice().map((row: number[], i) => row.slice().concat(t2[i]))
  // console.log('r2t2 =', r2t2)
  return [r1t1, r1t2, r2t1, r2t2]
}
