import * as cmp from '../utils/compare'
import estimateInitialRT from './estimateInitialRT'
import * as math from 'mathjs'
import { insertInto } from '../utils/mat'
import { nonlinearEstimate3dPoint } from './nonlinearEstimate3dPoint'
import linearEstimate3dPoint from './linearEstimate3dPoint'

/**
 ESTIMATE_RT_FROM_E from the Essential Matrix, we can compute  the relative RT
 between the two cameras
 Arguments:
 E - the Essential Matrix between the two cameras
 image_points - N measured points in each of the M images (NxMx2 matrix)
 K - the intrinsic camera matrix
 Returns:
 RT: The 3x4 matrix which gives the rotation and translation between the
 two cameras
 */

export default function estimateRTfromE(E: number[][], imagePoints: number[][][], K: number[][]): number[][] {
  // console.log('estimateRTfromE E =', E)
  // console.log('estimateRTfromE imagePoints.length =', imagePoints.length)
  // console.log('estimateRTfromE imagePoints[0] =', imagePoints[0])
  // console.log('estimateRTfromE K =', K)
  const temp_E = require('../check/temp/E.json')
  // console.log('temp_E =', temp_E)
  const temp_image_points = require('../check/temp/image_points.json')
  const temp_K = require('../check/temp/K.json')
  // console.log('E same temp_E =', cmp.isSameMat(E, temp_E))
  // console.log('imagePoints same temp_image_points =', imagePoints.map((point, idx) => {
  //   return cmp.isSameMat(imagePoints[idx], temp_image_points[idx])
  // }))
  // console.log('K same temp_K =', cmp.isSameMat(K, temp_K))
  // const e = require('../check/part_e/E')
  // const image_points = require('../check/part_e/image_points')
  // const k = require('../check/part_e/K')
  // console.log('E same e =', cmp.diffMat(E, e))
  // console.log('imagePoints same image_points =', cmp.diffMat(imagePoints[0], image_points[0]))
  // console.log('K same k =', cmp.diffMat(K, k))
  const rti = estimateInitialRT(E)
  // console.log('rti =', rti)
  const temp_rti = require('../check/temp/rti.json')
  const same = cmp.sameInitialRT(rti, temp_rti)
  // console.log('rti same temp_rti =', same)
  // const rtic = JSON.parse(JSON.stringify(rti))
  // rti[0] = rtic[same.found[0]]
  // rti[1] = rtic[same.found[1]]
  // rti[2] = rtic[same.found[2]]
  // rti[3] = rtic[same.found[3]]
  // console.log('rti =', rti)
  // console.log('rti same temp_rti =', cmp.sameInitialRT(rti, temp_rti))

  // const part_e_rti = require('../check/part_e/rti.json')
  // console.log('rti same part_e_rti =', cmp.sameInitialRT(rti, part_e_rti))
  const mrt = (math.zeros(3, 4) as math.Matrix).toArray() as number[][]
  insertInto(mrt, (math.identity(3) as math.Matrix).toArray() as number[][], 0, 0)
  // console.log('mrt =', mrt)
  const m = math.multiply(K, mrt) as number[][]
  // console.log('m =', m)
  // const temp_m = require('../check/temp/m.json')
  // console.log('m same temp_m =', cmp.isSameMat(m, temp_m))
  const sums = [0, 0, 0, 0]
  for (let i = 0; i < 4; i++) {
    const rt = rti[i]
    const r = rt.map((row: number[]) => row.slice(0, -1))
    const t = rt.map((row: number[]) => row.slice(-1))
    // console.log('r =', r)
    // console.log('t =', t)
    const mprt = (math.zeros(3, 4) as math.Matrix).toArray() as number[][]
    const rtransp = math.transpose(r)
    insertInto(mprt, rtransp, 0, 0)
    insertInto(mprt, math.multiply(math.multiply(rtransp, t), -1), 0, 3)
    const mp = math.multiply(K, mprt)
    // console.log('mp =', mp)
    const temp_mp = require('../check/temp/mp.json')
    // console.log('mp same temp_mp =', cmp.isSameMat(mp, temp_mp))
    for (let j = 0; j < imagePoints.length; j++) {
      const p = (math.ones(4) as math.Matrix).toArray() as number[]
      const est = nonlinearEstimate3dPoint(imagePoints[j], [m, mp])
      // console.log('est =', est)
      insertInto([p], [est], 0, 0)
      // console.log('p =', p)
      // throw new Error('exit')
      const P1 = math.multiply(m, p)
      const P2 = math.multiply(mp, p)
      // console.log('P1 =', P1)
      // console.log('P2 =', P2)
      if (Number(P1[2]) > 0) {
        sums[i] += 1
      }
      if (Number(P2[2]) > 0) {
        sums[i] += 1
      }
    }
  }
  // console.log('sums =', sums)
  const idx: number = sums.indexOf(Math.max(...sums))
  // console.log('idx =', idx)
  return rti[idx]
}