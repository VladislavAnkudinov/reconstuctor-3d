import * as math from 'mathjs'
import { Matrix } from 'mathjs'

/**
 JACOBIAN given a 3D point and its corresponding points in the image
 planes, compute the reprojection error vector and associated Jacobian
 Arguments:
 point_3d - the 3D point corresponding to points in the image
 camera_matrices - the camera projective matrices (Mx3x4 tensor)
 Returns:
 jacobian - the 2Mx3 Jacobian matrix
 */
export function jacobian(point3d: number[], cameraMatrices: number[][][]): number[][] {
  const x = 0
  const y = 1
  const z = 2
  const p = point3d
  const size = cameraMatrices.length
  const jac = (math.zeros(size * 2, 3) as Matrix).toArray() as number[][]
  for (let i = 0; i < size; i++) {
    const m = cameraMatrices[i]
    for (let j = 0; j < 2; j++) {
      const string = (2 * i) + j
      const mt = m[j]
      const mb = m[2]
      const b2 = (p[x] * mb[0] + p[y] * mb[1] + p[z] * mb[2] + 1.0 * mb[3]) ** 2
      for (let k = 0; k < 3; k++) {
        const gph = mt[k] * (p[x] * mb[0] + p[y] * mb[1] + p[z] * mb[2] + 1.0 * mb[3])
        const ghp = (p[x] * mt[0] + p[y] * mt[1] + p[z] * mt[2] + 1.0 * mt[3]) * mb[k]
        jac[string][k] = (gph - ghp) / b2
      }
    }
  }
  return jac
}