import * as math from 'mathjs'
import { insertInto } from '../utils/mat'
import { Matrix } from 'mathjs'
import svd from '../utils/svd'
import * as cmp from '../utils/compare'

/**
 LINEAR_ESTIMATE_3D_POINT given a corresponding points in different images,
 compute the 3D point is the best linear estimate
 Arguments:
 image_points - the measured points in each of the M images (Mx2 matrix)
 camera_matrices - the camera projective matrices (Mx3x4 tensor)
 Returns:
 point_3d - the 3D point
 */
export default function linearEstimate3dPoint(imagePoints: number[][], cameraMatrices: number[][][]): number[] {
  const pa = imagePoints[0]
  const pb = imagePoints[1]
  const ma1 = cameraMatrices[0][0]
  const ma2 = cameraMatrices[0][1]
  const ma3 = cameraMatrices[0][2]
  const mb1 = cameraMatrices[1][0]
  const mb2 = cameraMatrices[1][1]
  const mb3 = cameraMatrices[1][2]
  const x = 0
  const y = 1
  const a = (math.zeros(4, 4) as Matrix).toArray() as number[][]
  const insert0 = math.subtract(math.multiply(pa[x], ma3), ma1) as number[]
  const insert1 = math.subtract(math.multiply(pa[y], ma3), ma2) as number[]
  const insert2 = math.subtract(math.multiply(pb[x], mb3), mb1) as number[]
  const insert3 = math.subtract(math.multiply(pb[y], mb3), mb2) as number[]
  insertInto(a, [insert0], 0, 0)
  insertInto(a, [insert1], 1, 0)
  insertInto(a, [insert2], 2, 0)
  insertInto(a, [insert3], 3, 0)
  // console.log('a =', cmp.isSameMat(a, require('../check/temp/linear_estimate_3d_point-a.json')))
  const svda = svd(a)
  const ua = svda.u
  const sa = svda.q
  const va = svda.v
  // console.log('ua =', ua)
  // console.log('sa =', sa)
  // console.log('va =', va)
  const csa = require('../check/temp/sa')
  // console.log('csa =', csa)
  // console.log('ua =', cmp.isSameMatAbs(ua, require('../check/temp/ua')))
  // console.log('sa =', cmp.isSameVec(sa, require('../check/temp/sa')))
  // console.log('va =', cmp.isSameMatAbs(va, require('../check/temp/va')))
  const p = va[va.length - 1]
  // console.log('p =', p)
  const pn = math.divide(p, p[3]) as number[]
  // console.log('pn =', pn)
  const pnt = pn.slice(0, -1)
  // console.log('pnt =', pnt)
  // throw new Error('exit')
  return pnt
}