import linearEstimate3dPoint from './linearEstimate3dPoint'
import reprojectionError from './reprojectionError'
import { jacobian } from './jacobian'
import * as math from 'mathjs'
import * as cmp from '../utils/compare'

/**
 NONLINEAR_ESTIMATE_3D_POINT given a corresponding points in different images,
 compute the 3D point that iteratively updates the points
 Arguments:
 image_points - the measured points in each of the M images (Mx2 matrix)
 camera_matrices - the camera projective matrices (Mx3x4 tensor)
 Returns:
 point_3d - the 3D point
 */
export function nonlinearEstimate3dPoint(imagePoints: number[][], cameraMatrices: number[][][]): number[] {
  // console.log(
  //   'nonlinearEstimate3dPoint:imagePoints =',
  //   cmp.isSameMat(imagePoints, require('../check/temp/nonlinear_estimate_3d_point-image_points.json'))
  // )
  // console.log(
  //   'nonlinearEstimate3dPoint:cameraMatrices =',
  //   cameraMatrices.map((m, idx) => {
  //     return cmp.isSameMat(
  //       cameraMatrices[idx],
  //       require('../check/temp/nonlinear_estimate_3d_point-camera_matrices.json')[idx]
  //     )
  //   })
  // )
  let p = linearEstimate3dPoint(imagePoints, cameraMatrices)
  // console.log(
  //   'nonlinearEstimate3dPoint:p =',
  //   cmp.isSameVec(p, require('../check/temp/nonlinear_estimate_3d_point-p.json'))
  // )
  // throw new Error('exit')
  for (let i = 0; i < 10; i++) {
    const e = reprojectionError(p, imagePoints, cameraMatrices)
    // console.log('e =', e)
    const j = jacobian(p, cameraMatrices)
    const jt = math.transpose(j)
    const jtj = math.multiply(jt, j)
    const ijtj = math.inv(jtj)
    const ijtjjt = math.multiply(ijtj, jt)
    const ijtjjte = math.multiply(ijtjjt, e)
    p = math.subtract(p, ijtjjte) as number[]
  }
  return p
}
