import * as math from 'mathjs'
import { insertInto } from '../utils/mat'
import { MathArray, Matrix } from 'mathjs'

/**
 REPROJECTION_ERROR given a 3D point and its corresponding points in the image
 planes, compute the reprojection error vector and associated Jacobian
 Arguments:
 point_3d - the 3D point corresponding to points in the image
 image_points - the measured points in each of the M images (Mx2 matrix)
 camera_matrices - the camera projective matrices (Mx3x4 tensor)
 Returns:
 error - the 2Mx1 reprojection error vector
 */
export default function reprojectionError(point3d: number[], imagePoints: number[][], cameraMatrices: number[][][]): number[] {
  const size = cameraMatrices.length
  // console.log('size =', size)
  const p = (math.ones(4) as Matrix).toArray() as number[]
  // console.log('p =', p)
  insertInto([p], [point3d], 0, 0)
  // console.log('p =', p)
  const e = (math.zeros(size, 2) as Matrix).toArray() as number[][]
  // console.log('e =', e)
  for (let i = 0; i < size; i++) {
    const m = cameraMatrices[i]
    const pr = math.multiply(m, p)
    const prn = math.divide(pr, pr[2]) as number[]
    const prnt = prn.slice(0, -1)
    const pim = imagePoints[i]
    e[i] = math.subtract(prnt, pim) as number[]
  }
  // console.log('e =', e)
  const err = [].concat(...e)
  // console.log('err =', err)
  return err
}
