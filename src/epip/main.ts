import linearEstimate3dPoint from './geom/linearEstimate3dPoint'

const start = new Date()
import * as math from 'mathjs'
import { Matrix } from 'mathjs'
import * as cmp from './utils/compare'
import { plainToImg } from './utils/img'
import estimateInitialRT from './geom/estimateInitialRT'
import { checkEstimateInitialRT } from './data/check/check'
import { insertInto } from './utils/mat'
import partB from './data/check/partB'
import { imread } from './utils/io'
import reprojectionError from './geom/reprojectionError'
import { jacobian } from './geom/jacobian'
import { nonlinearEstimate3dPoint } from './geom/nonlinearEstimate3dPoint'
import estimateRTfromE from './geom/estimateRTfromE'
import { Frame } from './sfm/Frame'
import bundleAdjustment from './sfm/bundleAdjustment'

const runPipeline = true

async function main() {
  // Load the data
  // console.log(new Array(80).join('-'))
  // console.log('Load the data')
  // console.log(new Array(80).join('-'))
  const unitTestCameraMatrix = require('./data/unitTestCameraMatrix')
  // console.log('unitTestCameraMatrix.length =', unitTestCameraMatrix.length)
  const unitTestImageMatches = require('./data/unitTestImageMatches')
  // console.log('unitTestImageMatches.length =', unitTestImageMatches.length)
  const focalLength = 719.5459
  // console.log('focalLength =', focalLength)
  const matchesSubset = require('./data/statue/matchesSubset.json')
  // console.log('matchesSubset.length =', matchesSubset.length)
  const denseMatches = require('./data/statue/denseMatches')
  // console.log('denseMatches.length =', denseMatches.length)
  const fundamentalMatrices = require('./data/statue/fundamentalMatrices')
  // console.log('fundamentalMatrices.length =', fundamentalMatrices.length)

  let imHeight = 640
  let imWidth = 480

  if (!runPipeline) {
    const im0py: number[][][] = require('./data/check/images/im0py')
    const im0jimpPlain: number[] = require('./data/check/im0jimpPlain')
    const im0cvPlain: number[] = require('./data/check/im0cvPlain')
    const im0jimp = plainToImg(im0jimpPlain, im0py[0].length, im0py.length, true)
    const im0cv = plainToImg(im0cvPlain, im0py[0].length, im0py.length, true)
    // const l0 = Number(new Date())
    const im0cvn = await imread('../data/statue/images/B21.jpg')
    // const l1 = Number(new Date())
    // const im0cvn1 = await imread('./data/statue/images/B21.jpg')
    // const l2 = Number(new Date())
    // const im0cvn2 = await imread('./data/statue/images/B21.jpg')
    // const l3 = Number(new Date())
    // console.log('l3 - l2 =', l3 - l2)
    // console.log('l2 - l1 =', l2 - l1)
    // console.log('l1 - l0 =', l1 - l0)

    console.log('maxDistImg(im0py, im0jimp) =', cmp.maxDistImg(im0py, im0jimp))
    console.log('maxDistImg(im0py, im0cv) =', cmp.maxDistImg(im0py, im0cv))
    console.log('maxDistImg(im0py, im0cvn) =', cmp.maxDistImg(im0py, im0cvn))

    const im0 = im0py
    imHeight = im0.length
    imWidth = im0[0].length
  }

  if (!runPipeline) {

    // Part A: Computing the 4 initial R,T transformations from Essential Matrix
    console.log(new Array(80).join('-'))
    console.log('Part A: Check your matrices against the example R,T')
    console.log(new Array(80).join('-'))
    const K: number[][] = (math.identity(3) as Matrix).toArray() as number[][]
    console.log('K =', K)
    K[0][0] = K[1][1] = focalLength
    console.log('K =', K)
    const E = math.multiply(math.multiply(math.transpose(K), fundamentalMatrices[0]), K)
    console.log('E =', E)
    // console.log('imHeight =', imHeight)
    // console.log('imWidth =', imWidth)
    const exampleRT = [
      [0.9736, -0.0988, -0.2056, 0.9994],
      [0.1019, 0.9948, 0.0045, -0.0089],
      [0.2041, -0.0254, 0.9786, 0.0331]
    ]
    console.log('Example RT:\n', exampleRT)
    // console.log('Calc E:\n', E)
    // console.log('Check E:\n', checkEstimateInitialRT.E)
    // console.log('diff exampleRT and E =', cmp.diffMat(E, checkEstimateInitialRT.E))
    const estimatedRT = estimateInitialRT(E)
    // const estimatedRT = estimateInitialRT(checkEstimateInitialRT.E)
    console.log('')
    console.log('Estimated RT:\n', estimatedRT)
    console.log('checkEstimateInitialRT.res same estimatedRT =', cmp.sameInitialRT(checkEstimateInitialRT.res, estimatedRT).total)

    // Part B: Determining the best linear estimate of a 3D point
    console.log(new Array(80).join('-'))
    console.log('Part B: Check that the difference from expected point ')
    console.log('is near zero')
    console.log(new Array(80).join('-'))
    // camera_matrices = np.zeros((2, 3, 4))
    const cameraMatrices: number[][][] = [
      (math.zeros(3, 4) as Matrix).toArray() as number[][],
      (math.zeros(3, 4) as Matrix).toArray() as number[][]
    ]
    insertInto(cameraMatrices[0], K)
    insertInto(cameraMatrices[1], math.multiply(K, exampleRT))
    console.log('cameraMatrices =', cameraMatrices)
    console.log('same cameraMatrices[0] =', cmp.isSameMat(cameraMatrices[0], partB.cameraMatrices[0]))
    console.log('same cameraMatrices[1] =', cmp.isSameMat(cameraMatrices[1], partB.cameraMatrices_dumps[1]))
    // const unitTestMatches = matchesSubset[0][:, 0].reshape(2, 2)
    const unitTestMatches = math.reshape(matchesSubset[0].map((matches: number[]) => matches[0]), [2, 2])
    console.log('unitTestMatches =', unitTestMatches)
    const unit_test_matches = require('./check/part_b/unit_test_matches')
    console.log('unitTestMatches same unit_test_matches =', cmp.isSameMat(unitTestMatches, unit_test_matches))
    const estimated3dPoint = linearEstimate3dPoint(
      JSON.parse(JSON.stringify(unitTestMatches)),
      JSON.parse(JSON.stringify(cameraMatrices))
    )
    const expected_3d_point = require('./check/part_b/expected_3d_point')
    const estimated_3d_point = require('./check/part_b/estimated_3d_point')
    console.log('estimated3dPoint =', estimated3dPoint)
    console.log('estimated3dPoint same expected_3d_point =', cmp.isSameVec(estimated3dPoint, expected_3d_point))
    console.log('estimated3dPoint same estimated_3d_point =', cmp.isSameVec(estimated3dPoint, estimated_3d_point))
    const vecSum = (accumulator: number, currentValue: number) => Math.abs(accumulator) + Math.abs(currentValue)
    // const matSum = (accumulator: number[], currentValue: number[]) => accumulator.reduce(vecSum) + currentValue.reduce(vecSum)
    console.log('Difference: ', cmp.diffVec(estimated3dPoint, expected_3d_point).reduce(vecSum))

    // Part C: Calculating the reprojection error and its Jacobian
    console.log(new Array(80).join('-'))
    console.log('Part C: Check that the difference from expected error/Jacobian ')
    console.log('is near zero')
    console.log(new Array(80).join('-'))

    const estimatedError = reprojectionError(expected_3d_point, unitTestMatches, cameraMatrices)
    console.log('estimatedError =', estimatedError)
    const estimated_error = require('./check/part_c/estimated_error')
    console.log('estimatedError same estimated_error =', cmp.isSameVec(estimatedError, estimated_error))

    const expected_error = require('./check/part_c/expected_error')
    // console.log('Error Difference: ', cmp.diffVec(estimated_error, expected_error))
    console.log('Error Difference: ', cmp.diffVec(estimatedError, expected_error).reduce(vecSum))

    const estimatedJacobian = jacobian(expected_3d_point, cameraMatrices)
    console.log('estimatedJacobian =', estimatedJacobian)
    const estimated_jacobian = require('./check/part_c/estimated_jacobian')
    console.log('estimatedJacobian same estimated_jacobian =', cmp.isSameMat(estimatedJacobian, estimated_jacobian))

    const expected_jacobian = require('./check/part_c/expected_jacobian')
    // console.log('Jacobian Difference: ', cmp.diffMat(estimated_jacobian, expected_jacobian))
    console.log('Jacobian Difference: ', [].concat(...cmp.diffMat(estimatedJacobian, expected_jacobian)).reduce(vecSum))

    // Part D: Determining the best nonlinear estimate of a 3D point
    console.log(new Array(80).join('-'))
    console.log('Part D: Check that the reprojection error from nonlinear method')
    console.log('is lower than linear method')
    console.log(new Array(80).join('-'))

    const estimated3dPointLinear = linearEstimate3dPoint(
      JSON.parse(JSON.stringify(unitTestImageMatches)),
      JSON.parse(JSON.stringify(unitTestCameraMatrix))
    )
    console.log('estimated3dPointLinear =', estimated3dPointLinear)
    const estimated_3d_point_linear = require('./check/part_d/estimated_3d_point_linear')
    console.log('estimated3dPointLinear same estimated_3d_point_linear', cmp.isSameVec(estimated3dPointLinear, estimated_3d_point_linear))

    const estimated3dPointNonlinear = nonlinearEstimate3dPoint(
      JSON.parse(JSON.stringify(unitTestImageMatches)),
      JSON.parse(JSON.stringify(unitTestCameraMatrix))
    )
    console.log('estimated3dPointNonlinear =', estimated3dPointNonlinear)
    const estimated_3d_point_nonlinear = require('./check/part_d/estimated_3d_point_nonlinear')
    console.log('estimated3dPointNonlinear same estimated_3d_point_nonlinear', cmp.isSameVec(estimated3dPointNonlinear, estimated_3d_point_nonlinear))

    const vecNormSq = (accumulator: number, currentValue: number) => accumulator + currentValue ** 2

    const errorLinear = reprojectionError(
      estimated3dPointLinear,
      unitTestImageMatches,
      unitTestCameraMatrix
    )
    console.log('errorLinear =', errorLinear)
    console.log('Linear method error:', errorLinear.reduce(vecNormSq) ** 0.5)

    const errorNonlinear = reprojectionError(
      estimated3dPointNonlinear,
      unitTestImageMatches,
      unitTestCameraMatrix
    )
    console.log('errorNonlinear =', errorNonlinear)
    console.log('Nonlinear method error:', errorNonlinear.reduce(vecNormSq) ** 0.5)

    // Part E: Determining the correct R, T from Essential Matrix
    console.log(new Array(80).join('-'))
    console.log('Part E: Check your matrix against the example R,T')
    console.log(new Array(80).join('-'))
    const estimatedRTfromE = estimateRTfromE(E, [unitTestImageMatches.slice(0, 2)], K)
    console.log('Example RT:\n', exampleRT)
    console.log('')
    console.log('Estimated RT:\n', estimatedRTfromE)
    const estimated_RT = require('./check/part_e/estimated_RT')
    // console.log('estimatedRTfromE same estimated_RT', cmp.diffMat(estimatedRTfromE, estimated_RT))
    // console.log('estimatedRTfromE same exampleRT', cmp.diffMat(estimatedRTfromE, exampleRT))
    // console.log('estimated_RT same exampleRT', cmp.diffMat(estimated_RT, exampleRT))
  } else {
    // Part F: Run the entire Structure from Motion pipeline
    console.log(new Array(80).join('-'))
    console.log('Part F: Run the entire SFM pipeline')
    console.log(new Array(80).join('-'))

    const imagesCount = 4
    const matchesSubsetTranspose = matchesSubset.map((m: number[][]) => math.transpose(m))
    // console.log('matchesSubsetTranspose.length =', matchesSubsetTranspose.length)
    // console.log('matchesSubsetTranspose[0].length =', matchesSubsetTranspose[0].length)
    // console.log('matchesSubsetTranspose[0][0].length =', matchesSubsetTranspose[0][0].length)
    const frames: Frame[] = []
    const startFrames = new Date()
    for (let i = 0; i < imagesCount; i++) {
      frames[i] = new Frame(matchesSubsetTranspose[i], focalLength, fundamentalMatrices[i], imWidth, imHeight)
      console.log('frame, i =', i, ', same T =', cmp.isSameMat(frames[i].T, require('./check/temp/T_' + i)))
      const temp_structure = require('./check/temp/structure_' + i + '.json')
      const temp_motion = require('./check/temp/motion_' + i + '.json')
      console.log('frame, i =', i, ', same structure =', cmp.isSameMat(frames[i].structure, temp_structure))
      console.log(
        'frame, i =', i, ', same motion =',
        temp_motion.map((m: number[][], j: number) => cmp.isSameMat(frames[i].motion[j], temp_motion[j]))
      )
      // console.log('frames[', i, '] =', frames[i])
      bundleAdjustment(frames[i])
    }
    // console.log('frames =', frames)
    console.log('elapsed frames =', Number(new Date()) - Number(startFrames))
    process.exit(0)
    // frames[i] = Frame(matches_subset[i].T, focal_length,
    //   fundamental_matrices[i], im_width, im_height)
    // bundle_adjustment(frames[i])
    // merged_frame = merge_all_frames(frames)
    //
    // # Construct the dense matching
    // camera_matrices = np.zeros((2, 3, 4))
    // dense_structure = np.zeros((0, 3))
    // for i in range(len(frames) - 1):
    // matches = dense_matches[i]
    // camera_matrices[0, :, :] = merged_frame.K.dot(
    //   merged_frame.motion[i, :, :])
    // camera_matrices[1, :, :] = merged_frame.K.dot(
    //   merged_frame.motion[i + 1, :, :])
    // points_3d = np.zeros((matches.shape[1], 3))
    // use_point = np.array([True] * matches.shape[1])
    // for j in range(matches.shape[1]):
    // points_3d[j, :] = nonlinear_estimate_3d_point(
    //   matches[:, j].reshape((2, 2)), camera_matrices)
    // dense_structure = np.vstack((dense_structure, points_3d[use_point, :]))
    //
    // fig = plt.figure(figsize=(10, 10))
    // ax = fig.gca(projection='3d')
    // ax.scatter(dense_structure[:, 0], dense_structure[:, 1], dense_structure[:, 2],
    // c='k', depthshade=True, s=2)
    // ax.set_xlim(-5, 5)
    // ax.set_ylim(-5, 5)
    // ax.set_zlim(0, 10)
    // ax.view_init(-100, 90)
  }
  const end = new Date()
  console.log('elapsed =', Number(end) - Number(start))
}

main()
