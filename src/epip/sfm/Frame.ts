import * as math from 'mathjs'
import * as cmp from '../utils/compare'
import estimateRTfromE from '../geom/estimateRTfromE'
import { insertInto } from '../utils/mat'
import triangulate from './triangulate'

const range = (start: number, end: number): number[] => {
  const length = end - start
  return Array.from({length}, (_, i) => start + i)
}

// A class that stores information about multiple views of the scene
export class Frame {
  N: number
  matchIdx: number[][]
  matchPoints: number[][]
  K: number[][]
  E: number[][]
  T: number[][]
  motion: number[][][]
  structure: number[][]

  constructor(
    public matches: number[][],
    public focalLength: number,
    public F: number[][],
    public imWidth: number,
    public imHeight: number
  ) {
    // console.log('matches =', matches)

    this.N = matches.length
    console.log('N =', this.N)
    this.matchIdx = [
      range(0, this.N),
      range(this.N, this.N * 2)
    ]
    // console.log('matchIdx =', this.matchIdx)
    this.matchPoints = [
      ...this.matches.map((m: number[]) => m.slice(0, 2)),
      ...this.matches.map((m: number[]) => m.slice(2, 4))
    ]
    // console.log('matchPoints =', this.matchPoints)

    this.K = (math.identity(3) as math.Matrix).toArray() as number[][]
    this.K[0][0] = this.K[1][1] = focalLength
    console.log('K =', this.K)
    this.E = math.multiply(math.multiply(math.transpose(this.K), F), this.K)
    console.log('E =', this.E)
    const matchesReshape: number[][][] = this.matches.map((m: number[]) => [m.slice(0, 2), m.slice(2, 4)])
    // console.log('matchesReshape =', matchesReshape)
    this.T = estimateRTfromE(this.E, matchesReshape, this.K)
    console.log('T =', JSON.stringify(this.T))
    this.motion = [
      (math.zeros(3, 4) as math.Matrix).toArray() as number[][],
      (math.zeros(3, 4) as math.Matrix).toArray() as number[][]
    ]
    insertInto(this.motion[0], (math.identity(3) as math.Matrix).toArray() as number[][], 0, 0)
    insertInto(this.motion[1], this.T, 0, 0)
    this.structure = triangulate(this)
    // console.log('structure =', JSON.stringify(this.structure))
  }
}

// def __init__(self, matches, focal_length, F, im_width, im_height):
// self.focal_length = focal_length
// self.im_height = im_height
// self.im_width = im_width
// self.matches = matches
//
// self.N = matches.shape[0]
// self.match_idx = np.array([np.arange(self.N),
//   np.arange(self.N, 2 * self.N)])
// self.match_points = np.vstack((matches[:,:2], matches[:,2:]))
//
// self.K = np.eye(3)
// self.K[0,0] = self.K[1,1] = focal_length
// self.E = self.K.T.dot(F).dot(self.K)
// self.T = estimate_RT_from_E(self.E, matches.reshape((-1,2,2)), self.K)
//
// self.motion = np.zeros((2,3,4))
// self.motion[0,:,:-1] = np.eye(3)
// self.motion[1,:,:] = self.T
// self.structure = triangulate(self)