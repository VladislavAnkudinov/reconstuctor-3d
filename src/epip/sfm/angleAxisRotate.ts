import * as math from 'mathjs'
import * as cmp from '../utils/compare'
import crossProductMat from './crossProductMat'

/**
 ANGLE_AXIS_ROTATE
 Arguments:
 angle_axis - the angle axis representation of the rotation
 pt - a matrix containing points
 Returns:
 new_pts - a matrix containing the points after the rotation is applied
 */

export default function angleAxisRotate(angleAxis: number[], pt: number[][]): number[][] {
  // console.log('angleAxis =', angleAxis)
  // console.log('pt.length =', pt.length)
  // console.log('pt[0].length =', pt[0].length)
  // console.log('same =', cmp.isSameMat(pt, require('../check/temp/pt.json')))
  const aa = [angleAxis]
  // console.log('aa =', aa)
  let theta2: number | number[][] = math.multiply(aa, math.transpose(aa))
  // console.log('theta2 =', theta2)
  theta2 = theta2[0][0]
  // console.log('theta2 =', theta2)
  const result: number[][] = JSON.parse(JSON.stringify(pt))
  if (theta2 > 0) {
    // console.log('gonna calc result 2')
    const theta = Math.sqrt(theta2)
    const w = [aa[0].map((val: number) => val / theta)]
    const cosTheta = Math.cos(theta)
    const sinTheta = Math.sin(theta)
    const wCrossPt = math.multiply(crossProductMat(w[0]), pt)
    const wDotPt = math.multiply(w, pt)
    result.forEach((row: number[], i) => {
      row.forEach((col: number, j: number) => {
        result[i][j] += wCrossPt[i][j]
      })
    })
    // result = pt * cos_theta + w_cross_pt * sin_theta + (w.T * (1 - cos_theta)).dot(w_dot_pt)
    const ptCosTheta = pt.map((row: number[]) => {
      return row.map((value: number) => {
        return value * cosTheta
      })
    })
    const wCrossPtSinTheta = wCrossPt.map((row: number[]) => {
      return row.map((value: number) => {
        return value * sinTheta
      })
    })
    const oneMinusCosTheta = 1 - cosTheta
    const wTransposeOneMinusCosTheta = math.transpose(w).map((row: number[]) => {
      return row.map((value: number) => {
        return value * oneMinusCosTheta
      })
    })
    const wTransposeOneMinusCosThetaDotWDotPt = math.multiply(wTransposeOneMinusCosTheta, wDotPt)
    result.forEach((row: number[], i) => {
      row.forEach((col: number, j: number) => {
        result[i][j] = ptCosTheta[i][j] + wCrossPtSinTheta[i][j] + wTransposeOneMinusCosThetaDotWDotPt[i][j]
      })
    })
    // console.log('result2 =', result)
    // process.exit()
  } else {
    const cpm = crossProductMat(aa[0])
    // console.log('cpm =', cpm)
    const wCrossPt = math.multiply(cpm, pt)
    // console.log('wCrossPt =', wCrossPt)
    result.forEach((row: number[], i) => {
      row.forEach((col: number, j: number) => {
        result[i][j] += wCrossPt[i][j]
      })
    })
  }
  // console.log('same result =', cmp.isSameMat(result, require('../check/temp/result.json')))
  // process.exit(0)
  return result
}