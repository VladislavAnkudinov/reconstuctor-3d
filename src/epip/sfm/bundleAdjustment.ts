import { Frame } from './Frame'
import * as math from 'mathjs'
import { getMat, insertInto, sumSquares } from '../utils/mat'
import rotationMatrixToAngleAxis from './rotationMatrixToAngleAxis'
import { diffVec, isSameMat, isSameMatVec } from '../utils/compare'
import reprojectionErrorMotStr from './reprojectionErrorMotStr'
import * as fs from 'fs'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import LM from 'ml-levenberg-marquardt'

const scipy = require('scipy')
const fmin = require('fmin')

/**
 BUNDLE_ADJUSTMENT
 Arguments:
 frame - the information about multiple views of the scene
 Returns:
 Nothing; updates the motion and structure information
 */

export default function bundleAdjustment(frame: Frame): void {
  console.log('frame.motion.shape =', frame.motion.length, frame.motion[0].length, frame.motion[0][0].length)
  const numCameras = frame.motion.length
  console.log('numCameras =', numCameras)
  const motionAngleAxis: number[][][] = Array.from(Array(numCameras).keys()).map(() => getMat(math.zeros(3, 2)))
  // console.log('motionAngleAxis =', motionAngleAxis)
  for (let i = 0; i < numCameras; i++) {
    const angleAxis = rotationMatrixToAngleAxis(frame.motion[i])
    insertInto(motionAngleAxis[i], math.transpose([angleAxis]), 0, 0)
    insertInto(motionAngleAxis[i], frame.motion[i].map((row: number[]) => row.slice(-1)), 0, 1)
  }
  console.log('motionAngleAxis =', motionAngleAxis)
  console.log('frame.structure.shape =', frame.structure.length, frame.structure[0].length)
  const px = 0
  const py = 0
  // const errors = reprojectionErrorMotStr(frame.matchIdx, frame.matchPoints, frame.focalLength, px, py, motionAngleAxis, frame.structure)
  // console.log('errors =', errors)
  function flatten(motionAngleAxis: number[][][], structure: number[][]) {
    return [...motionAngleAxis.flat().flat(), ...frame.structure.flat()]
  }

  function restore(X: number[], numCameras: number): {
    motionAngleAxis: number[][][],
    structure: number[][]
  } {
    const cut = 3 * 2 * numCameras
    // const motionAngleAxis: number[][][] = []
    // X.slice(0, cut).forEach((val, idx) => {
    //   const cam = Math.floor(idx / (3 * 2))
    //   const base = idx - cam * (3 * 2)
    //   const i = Math.floor(base / 2)
    //   const j = base % 2
    //   motionAngleAxis[cam] = motionAngleAxis[cam] || []
    //   motionAngleAxis[cam][i] = motionAngleAxis[cam][i] || []
    //   motionAngleAxis[cam][i][j] = val
    // })
    const camData = X.slice(0, cut)
    // console.log('camData.length =', camData.length)
    const motionAngleAxis: number[][][] = Array.from(Array(numCameras).keys()).map((cam: number) => {
      return Array.from(Array(3).keys()).map((i: number) => {
        const cami = 2 * (cam * 3 + i)
        return camData.slice(cami, cami + 2)
      })
    })
    // const structure: number[][] = []
    // X.slice(cut, X.length).forEach((val, idx) => {
    //   const i = Math.floor(idx / 3)
    //   const j = idx % 3
    //   structure[i] = structure[i] || []
    //   structure[i][j] = val
    // })
    const structData = X.slice(cut, X.length)
    const colSize = structData.length / 3
    const structure: number[][] = Array.from(Array(colSize).keys()).map((i: number) => {
      const ij = i * 3
      return structData.slice(ij, ij + 3)
    })
    // console.log('structure =', structure)
    return {
      motionAngleAxis,
      structure
    }
  }

  const X = flatten(motionAngleAxis, frame.structure)
  console.log('X =', X)
  const restored = restore(X, numCameras)
  console.log('restore.motionAngleAxis =', restored.motionAngleAxis)
  console.log('restore.structure =', restored.structure)
  console.log('same motionAngleAxis =', isSameMatVec(motionAngleAxis, restored.motionAngleAxis))
  console.log('frame.structure.length =', frame.structure.length)
  console.log('restored.structure.length =', restored.structure.length)
  console.log('same structure =', isSameMat(frame.structure, restored.structure))
  // process.exit()
  let ctr = 0
  const losses: number[] = []

  function loss(X: number[]) {
    ctr += 1
    // const x = X[0], y = X[1]
    // return Math.sin(y) * x + Math.sin(x) * y + x * x + y * y
    const {motionAngleAxis, structure} = restore(X, numCameras)
    const errors: number[] = reprojectionErrorMotStr(frame.matchIdx, frame.matchPoints, frame.focalLength, px, py, motionAngleAxis, structure)
    const err = sumSquares(errors)
    // console.log('ctr =', ctr, 'err =', err)
    if (ctr % 100 === 0) {
      losses.push(err)
    }
    return err
  }

  // const fittedParams = LM(data, loss, {initialValues: X});
  // console.log('Object.keys(scipy) =', Object.keys(scipy))
  // console.log('scipy =', scipy)
  // process.exit()
  console.log('wait about a minute...')
  const startSolution = new Date()
  const solution = fmin.nelderMead(loss, X)
  const optVal = solution.x
  const opt_val = require('../check/temp/opt_val.json')
  console.log('solution =', solution)
  console.log('optVal =', diffVec(optVal, opt_val))
  console.log('loss(optVal) =', loss(optVal))
  console.log('loss(opt_val) =', loss(opt_val))
  console.log('elapsed solution =', Number(new Date()) - Number(startSolution))
  fs.writeFileSync('losses.csv', losses.join('\n'))
  process.exit()
}