import { getMat } from '../utils/mat'
import * as math from 'mathjs'

/**
 CROSS_PRODUCT_MAT
 Arguments:
 a - a 3x1 vector
 Returns:
 m - the corresponding cross-product matrix [a]_x
 */
export default function crossProductMat(a: number[]): number[][] {
  // console.log('a =', a)
  const m = getMat(math.zeros(3, 3))
  m[1][0] = a[2]
  m[0][1] = -a[2]
  m[2][0] = -a[1]
  m[0][2] = a[1]
  m[2][1] = a[0]
  m[1][2] = -a[0]
  return m
}
