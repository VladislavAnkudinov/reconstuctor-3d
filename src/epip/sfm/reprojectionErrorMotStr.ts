import * as cmp from '../utils/compare'
import * as math from 'mathjs'
import angleAxisRotate from './angleAxisRotate'
import { isSameMat, isSameVec } from '../utils/compare'

/**
 REPROJECTION_ERROR_MOT_STR
 Arguments:
 match_idx - a MxN matrix corresponding to what indices of the match_points
 are visible in each of the M cameras
 match_points - the pixel locations in the image
 f - the focal length
 px - the principal component in the x direction
 py - the principal component in the y direction
 motion - the camera's motion tensor
 structure - the current 3D point estimate of the scene
 Returns:
 errors - the reprojection error vector
 */
export default function reprojectionErrorMotStr(
  matchIdx: number[][],
  matchPoints: number[][],
  f: number,
  px: number,
  py: number,
  motion: number[][][],
  structure: number[][]
): number[] {
  const N = matchIdx.length
  // console.log('N =', N)
  const errors: number[][] = []
  for (let i = 0; i < N; i++) {
    // console.log('errors, i =', i)
    // matchIdx[i][2] = -3
    // console.log('matchIdx[i] =', matchIdx[i])
    const validPts: boolean[] = matchIdx[i].map((idx: number) => idx >= 0)
    // console.log('validPts =', validPts)
    const validIdx: number[] = matchIdx[i].filter((idx: number) => idx >= 0)
    const validPtsIdx: number[] = []
    validPts.forEach((val, idx) => {
      if (!val) return
      validPtsIdx.push(idx)
    })
    // console.log('validIdx =', validIdx)
    // const RP = angle_axis_rotate(motion[i, :, 0], structure[valid_pts,:].T)
    // console.log('motion[i] =', motion[i])
    // console.log('motion[i][0] =', motion[i].map((row: number[]) => row[0]))
    // process.exit()
    // const pt = math.transpose(validIdx.map((p: number) => structure[p]))
    const pt = math.transpose(validPtsIdx.map((p: number) => structure[p]))
    if (i === 1) {
      // console.log('structure =', structure.length)
      // console.log('validIdx =', validIdx)
      // console.log('validPts =', validPts)
      // console.log('pt =', pt)
    }
    const RP = angleAxisRotate(motion[i].map((row: number[]) => row[0]), pt)
    // console.log('RP =', RP)
    const trxA = RP[0]
    // console.log('trxA =', trxA)
    const trxB = motion[i][0][1]
    // console.log('trxB =', trxB)
    const TRX = trxA.map((val: number) => val + trxB)
    // console.log('TRX =', TRX)
    // console.log('same =', isSameVec(TRX, require('../check/temp/TRX.json')))
    const TRY = RP[1].map((val: number) => val + motion[i][1][1])
    // console.log('TRY =', TRY)
    const TRZ = RP[2].map((val: number) => val + motion[i][2][1])
    // console.log('TRZ =', TRZ)
    const TRXoZ = TRX.map((val: number, idx: number) => val / TRZ[idx])
    // console.log('TRXoZ =', TRXoZ)
    // console.log('same =', isSameVec(TRXoZ, require('../check/temp/TRXoZ.json')))
    const TRYoZ = TRY.map((val: number, idx: number) => val / TRZ[idx])
    // console.log('TRYoZ =', TRYoZ)
    // console.log('same =', isSameVec(TRYoZ, require('../check/temp/TRYoZ.json')))
    const x = math.add(math.multiply(TRXoZ, f), px) as number[]
    // console.log('x =', x)
    // console.log('same =', isSameVec(x, require('../check/temp/x.json')))
    const y = math.add(math.multiply(TRYoZ, f), py) as number[]
    // console.log('y =', y)
    // console.log('same =', isSameVec(y, require('../check/temp/y.json')))
    const ox: number[] = validIdx.map((idx: number) => matchPoints[idx]).map((row: number[]) => row[0])
    // console.log('ox =', ox)
    // console.log('same =', isSameVec(ox, require('../check/temp/ox.json')))
    const oy: number[] = validIdx.map((idx: number) => matchPoints[idx]).map((row: number[]) => row[1])
    // console.log('oy =', oy)
    // console.log('same =', isSameVec(oy, require('../check/temp/oy.json')))
    const xox = math.subtract(x, ox) as number[]
    const yoy = math.subtract(y, oy) as number[]
    if (!errors.length) {
      errors.push(xox, yoy)
    } else {
      errors[0] = [].concat(errors[0], xox)
      errors[1] = [].concat(errors[1], yoy)
    }
    // console.log('errors =', errors)
    // if (i === 1) process.exit()
  }
  // console.log('errors =', errors)
  // console.log('same =', isSameMat(errors, require('../check/temp/errors.json')))
  // process.exit()
  return errors.flat()
}