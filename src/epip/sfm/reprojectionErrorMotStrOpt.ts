import * as math from 'mathjs'

/**
 REPROJECTION_ERROR_MOT_STR_OPT
 Arguments:
 mot_str - the motion and structure matrices flattened into one vector
 this allows us to use the builtin nonlinear optimization methods
 match_idx - a MxN matrix corresponding to what indices of the match_points
 are visible in each of the M cameras
 match_points - the pixel locations in the image
 f - the focal length
 px - the principal component in the x direction
 py - the principal component in the y direction
 Returns:
 errors - the reprojection error vector
 */
import reprojectionErrorMotStr from './reprojectionErrorMotStr'
import { reshape } from '../utils/mat'

export default function reprojectionErrorMotStrOpt(
  motStr: number[],
  matchIdx: number[][],
  matchPoints: number[][],
  f: number,
  px: number,
  py: number
): number[] {
  throw new Error('Not implemented')
  const numCameras = matchIdx.length
  const cut = 3 * 2 * numCameras
  const structure = reshape(motStr.slice(cut, motStr.length), [-1, 3]) as number[][]
  const motion = reshape(motStr.slice(0, cut), [-1, 3, 2]) as number[][][]
  const error = reprojectionErrorMotStr(matchIdx, matchPoints, f, px, py, motion, structure)
  return error
}