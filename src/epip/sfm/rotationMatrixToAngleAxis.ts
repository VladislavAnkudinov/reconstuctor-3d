/**
 ROTATION_MATRIX_TO_ANGLE_AXIS
 Arguments:
 R - a rotation matrix
 Returns:
 angle_axis - the angle axis representation of the rotation
 */

export default function rotationMatrixToAngleAxis(R: number[][]): number[] {
  // console.log('R =', R)
  const angleAxis = [0, 0, 0]
  // console.log('angelAxis =', angleAxis)
  angleAxis[0] = R[2][1] - R[1][2]
  angleAxis[1] = R[0][2] - R[2][0]
  angleAxis[2] = R[1][0] - R[0][1]
  // console.log('angelAxis =', angleAxis)
  const cosTheta = Math.min(Math.max((R[0][0] + R[1][1] + R[2][2] - 1.0) / 2.0, -1.0), 1.0)
  const sinTheta = Math.min((angleAxis[0] ** 2 + angleAxis[1] ** 2 + angleAxis[2] ** 2) ** 0.5 / 2, 1.0)
  // console.log('cosTheta =', cosTheta)
  // console.log('sinTheta =', sinTheta)
  const theta = Math.atan2(sinTheta, cosTheta)
  // console.log('theta =', theta)
  const kThreshold = 1e-12
  if ((sinTheta > kThreshold) || (sinTheta < -kThreshold)) {
    const r = theta / (2.0 * sinTheta)
    angleAxis.forEach((val, idx) => angleAxis[idx] = val * r)
    // console.log('angelAxis =', angleAxis)
    // process.exit()
    return angleAxis
  }
  if (cosTheta > 0) {
    angleAxis.forEach((val, idx) => angleAxis[idx] = val / 2)
    // console.log('angelAxis =', angleAxis)
    // process.exit()
    return angleAxis
  }
  // console.log('angelAxis =', angleAxis)
  const invOneMinusCosTheta = 1.0 / (1.0 - cosTheta)
  // console.log('invOneMinusCosTheta =', invOneMinusCosTheta)
  for (let i = 0; i < 3; i++) {
    // console.log('i =', i)
    angleAxis[i] = theta * Math.sqrt((R[i][i] - cosTheta) * invOneMinusCosTheta)
    if ((sinTheta < 0 && angleAxis[i] > 0) || (sinTheta > 0 && angleAxis[i] < 0)) {
      angleAxis.forEach((val, idx) => angleAxis[idx] = -val)
    }
  }
  // console.log('angelAxis =', angleAxis)
  // process.exit()
  return angleAxis
}