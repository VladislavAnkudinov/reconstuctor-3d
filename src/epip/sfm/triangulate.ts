import { Frame } from './Frame'
import * as math from 'mathjs'
import * as cmp from '../utils/compare'
import { getMat } from '../utils/mat'
import { nonlinearEstimate3dPoint } from '../geom/nonlinearEstimate3dPoint'

/**
 TRIANGULATE
 Arguments:
 frame - information about multiple views of the scene
 Returns:
 structure - the 3D points of the scene
 */
export default function triangulate(frame: Frame): number[][] {
  const numCameras = frame.matchIdx.length
  // console.log('numCameras =', numCameras)
  const numPoints = frame.matchIdx[0].length
  // console.log('numPoints =', numPoints)
  const structure = getMat(math.zeros(numPoints, 3))
  // console.log('structure =', structure)
  const allCameraMatrices = Array.from(Array(numCameras).keys()).map(() => getMat(math.zeros(3, 4)))
  // console.log('allCameraMatrices =', allCameraMatrices)
  for (let i = 0; i < numCameras; i++) {
    allCameraMatrices[i] = math.multiply(frame.K, frame.motion[i])
    // console.log('allCameraMatrices =', allCameraMatrices)
  }
  for (let i = 0; i < numPoints; i++) {
    // console.log('frame.matchIdx =', frame.matchIdx)
    const match = frame.matchIdx.map((row: number[]) => row[i])
    // console.log('match =', match)
    // valid_cameras = np.where(frame.match_idx[:,i] >= 0)[0]
    const validCameras: number[] = match.reduce((filtered, m, idx) => {
      if (m >= 0) {
        filtered.push(idx)
      }
      return filtered
    }, [])
    // console.log('validCameras =', validCameras)
    const cameraMatrices: number[][][] = validCameras.map((validIdx) => allCameraMatrices[validIdx])
    // console.log('cameraMatrices =', cameraMatrices)
    const x = getMat(math.zeros(validCameras.length, 2))
    // console.log('x =', x)
    validCameras.forEach((c, ctr) => {
      // console.log('ctr =', ctr)
      // console.log('c =', c)
      x[ctr] = frame.matchPoints[frame.matchIdx[c][i]]
      // console.log('x =', x)
    })
    structure[i] = nonlinearEstimate3dPoint(x, cameraMatrices)
  }
  // console.log('structure =', structure)
  // console.log('same =', cmp.isSameMat(structure, require('../check/temp/structure.json')))
  return structure
}