export function diffMat(a: number[][], b: number[][]): number[][] {
  return a.map((row: number[], i: number) => {
    return row.map((elem: number, j: number) => {
      return a[i][j] - b[i][j]
    })
  })
}

export function diffMatAbs(a: number[][], b: number[][]): number[][] {
  return a.map((row: number[], i: number) => {
    return row.map((elem: number, j: number) => {
      return Math.abs(a[i][j]) - Math.abs(b[i][j])
    })
  })
}

export function diffVec(a: number[], b: number[]): number[] {
  return a.map((row: number[] | number, i: number) => {
    return a[i] - b[i]
  })
}

export function isDiffMat(a: number[][], b: number[][], maxDiff = 1e-7): boolean {
  const maxDiffAbs = Math.abs(maxDiff)
  return a.some((row: number[], i: number) => {
    return row.some((elem: number, j: number) => {
      return Math.abs(a[i][j] - b [i][j]) > maxDiffAbs
    })
  })
}

export function isDiffVec(a: number[], b: number[], maxDiff = 1e-7): boolean {
  const maxDiffAbs = Math.abs(maxDiff)
  return a.some((row: number[] | number, i: number) => {
    return Math.abs(a[i] - b[i]) > maxDiffAbs
  })
}

export function zeroMat(m: number[][], maxVal = 1e-7): boolean[][] {
  const maxValSq = maxVal * maxVal
  return m.map((row: number[]) => {
    return row.map((elem: number) => {
      return elem * elem <= maxValSq
    })
  })
}

export function zeroVec(v: number[], maxVal = 1e-7): boolean[] {
  const maxValSq = maxVal * maxVal
  return v.map((elem: number) => {
    return elem * elem <= maxValSq
  })
}

export function isZeroMat(m: number[][], maxVal = 1e-7): boolean {
  const maxValSq = maxVal * maxVal
  return !m.some((row: number[]) => {
    return row.some((elem: number) => {
      return elem * elem > maxValSq
    })
  })
}

export function isZeroVec(v: number[], maxVal = 1e-7): boolean {
  const maxValSq = maxVal * maxVal
  return !v.some((elem: number) => {
    return elem * elem > maxValSq
  })
}

export function sameMat(a: number[][], b: number[][], maxDiff = 1e-7): boolean[][] {
  return zeroMat(diffMat(a, b), maxDiff)
}

export function sameVec(a: number[], b: number[], maxDiff = 1e-7): boolean[] {
  return zeroVec(diffVec(a, b), maxDiff)
}

export function isSameMat(a: number[][], b: number[][], maxDiff = 1e-7): boolean {
  return isZeroMat(diffMat(a, b), maxDiff)
}

export function isSameMatVec(a: number[][][], b: number[][][], maxDiff = 1e-7): boolean[] {
  return a.map((m, idx) => isSameMat(m, b[idx]))
}

export function isSameMatAbs(a: number[][], b: number[][], maxDiff = 1e-7): boolean {
  return isZeroMat(diffMatAbs(a, b), maxDiff)
}

export function isSameVec(a: number[], b: number[], maxDiff = 1e-7): boolean {
  return isZeroVec(diffVec(a, b), maxDiff)
}

export function diffImg(a: number[][][], b: number[][][]): number[][][] {
  console.log('a[0].lenght =', a[0].length)
  console.log('a[1].lenght =', a[1].length)
  console.log('a[2].lenght =', a[2].length)
  console.log('b[0].lenght =', b[0].length)
  console.log('b[1].lenght =', b[1].length)
  console.log('b[2].lenght =', b[2].length)
  return a.map((row: number[][], i: number) => {
    console.log('row.length =', row.length, 'i =', i)
    return row.map((elem: number[], j: number) => {
      return elem.map((value: number, c: number) => {
        return a[i][j][c] - b[i][j][c]
      })
    })
  })
}

export function maxDiffImg(a: number[][][], b: number[][][]): number {
  let maxDiff = 0
  a.forEach((row: number[][], i: number) => {
    row.forEach((elem: number[], j: number) => {
      elem.forEach((value: number, c: number) => {
        const diff = a[i][j][c] - b [i][j][c]
        if (Math.abs(diff) > maxDiff) {
          maxDiff = diff
        }
      })
    })
  })
  return maxDiff
}

export function sumDiffImg(a: number[][][], b: number[][][]): number {
  let sumDiff = 0
  a.forEach((row: number[][], i: number) => {
    row.forEach((elem: number[], j: number) => {
      elem.forEach((value: number, c: number) => {
        const diff = a[i][j][c] - b [i][j][c]
        sumDiff += diff
      })
    })
  })
  return sumDiff
}

export function sumAbsDiffImg(a: number[][][], b: number[][][]): number {
  let sumDiff = 0
  a.forEach((row: number[][], i: number) => {
    row.forEach((elem: number[], j: number) => {
      elem.forEach((value: number, c: number) => {
        const diff = a[i][j][c] - b [i][j][c]
        sumDiff += Math.abs(diff)
      })
    })
  })
  return sumDiff
}

export function distImg(a: number[][][], b: number[][][]): number[][] {
  return a.map((row: number[][], i: number) => {
    return row.map((elem: number[], j: number) => {
      let squareDist = 0
      elem.forEach((value: number, c: number) => {
        squareDist += (a[i][j][c] - b[i][j][c]) ** 2
      })
      return squareDist ** 0.5
    })
  })
}

export function maxDistImg(a: number[][][], b: number[][][]): number {
  let maxDist = -Infinity
  a.forEach((row: number[][], i: number) => {
    row.forEach((elem: number[], j: number) => {
      let squareDist = 0
      elem.forEach((value: number, c: number) => {
        squareDist += (a[i][j][c] - b[i][j][c]) ** 2
      })
      const dist = squareDist ** 0.5
      if (dist > maxDist) {
        maxDist = dist
      }
    })
  })
  return maxDist
}

export function sumDistImg(a: number[][][], b: number[][][]): number {
  let sumDist = 0
  a.forEach((row: number[][], i: number) => {
    row.forEach((elem: number[], j: number) => {
      let squareDist = 0
      elem.forEach((value: number, c: number) => {
        squareDist += (a[i][j][c] - b[i][j][c]) ** 2
      })
      sumDist += squareDist ** 0.5
    })
  })
  return sumDist
}

export type compareInitialRTResult = {
  found: number[]
  checks: boolean[]
  total: boolean
}

export function sameInitialRT(a: number[][][], b: number[][][]): compareInitialRTResult {
  const allowed = [0, 1, 2, 3]
  const found: number[] = []
  const checks = [false, false, false, false].map(
    (val: boolean, idx: number) => {
      for (let i = 0; i < allowed.length; i++) {
        if (!isSameMat(a[idx], b[allowed[i]])) continue
        found.push(allowed.splice(i, 1)[0])
        return true
      }
      return false
    }
  )
  const total = !checks.some((check: boolean) => !check)
  return {
    found,
    checks,
    total
  }
}
