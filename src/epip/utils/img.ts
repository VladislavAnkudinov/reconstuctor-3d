export function plainToImg(plain: number[], width: number, height: number, alpha = true): number[][][] {
  const numberOfChannels = alpha ? 4 : 3
  const img: number[][][] = []
  plain.forEach((el: number, idx: number) => {
    const c = idx % numberOfChannels
    if (c === numberOfChannels - 1) return
    const j = Math.floor(idx / numberOfChannels) % width
    const i = Math.floor(idx / (numberOfChannels * width))
    img[i] = img[i] || []
    img[i][j] = img[i][j] || []
    img[i][j][c] = el
  })
  return img
}
