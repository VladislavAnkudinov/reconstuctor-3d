import * as fs from 'fs'
import path from 'path'
import Jimp from 'jimp'
import { plainToImg } from './img'
import * as cmp from './compare'
import { loadImage } from 'canvas'
import { waitForCv, cv } from './opencv'

const im0jimpPlain: number[] = require('../data/check/im0jimpPlain')

export function getAbsPath(relativePath: string): string {
  return path.resolve(path.dirname(__filename), relativePath)
}

export function readFileSyncRelativePath(relativePath: string): string {
  return fs.readFileSync(getAbsPath(relativePath)).toString()
}

export function writeFileSyncRelativePath(relativePath: string, str: string): void {
  fs.writeFileSync(getAbsPath(relativePath), str)
}

export function writePlainArrayAsJson(relativePath: string, plainArray: number[]): void {
  writeFileSyncRelativePath(relativePath, JSON.stringify(plainArray, null, 2))
}

export async function getJimp(relativePath: string, alpha = true): Promise<{
  read: Jimp,
  width: number,
  height: number,
  plain: number[],
  im: number[][][]
}> {
  const read: Jimp = await Jimp.read(getAbsPath(relativePath))
  const width = read.bitmap.width
  const height = read.bitmap.height
  const plain = read.bitmap.data.toJSON().data
  const im: number[][][] = plainToImg(plain, width, height, alpha)
  return {read, width, height, plain, im}
}

export async function populateJimpFile(): Promise<void> {
  const jimp = await getJimp('../data/statue/images/B21.jpg')
  console.log('same =', cmp.isSameVec(im0jimpPlain, jimp.plain))
  writePlainArrayAsJson('../data/check/im0jimpPlain.json', jimp.plain)
}

export async function imread(relativePath: string): Promise<number[][][]> {
  const image = await loadImage(getAbsPath(relativePath))
  await waitForCv
  const src = cv.imread(image)
  const plain: number[] = []
  src.data.forEach((d: number) => plain.push(d))
  src.delete()
  return plainToImg(plain, image.width, image.height)
}
