import * as math from 'mathjs'

export function insertInto(into: number[][], from: number[][], atRow = 0, atColumn = 0): void {
  from.forEach((row: number[], i: number) => {
    row.forEach((elem: number, j: number) => {
      into[atRow + i][atColumn + j] = elem
    })
  })
}

export function getVat(v: number[] | number[][] | math.Matrix): number[] {
  return (v as math.Matrix).toArray() as number[]
}

export function getMat(m: number[] | number[][] | math.Matrix): number[][] {
  return (m as math.Matrix).toArray() as number[][]
}

export function reshape(m: number[] | number[][] | number[][][], shape: number[]): number[] | number[][] | number[][][] {
  throw new Error('Not implemented')
  return m
}

export function sumSquares(vec: number[]): number {
  return vec.reduce((a, num) => a + ((2 * num) ** 2), 0);
}