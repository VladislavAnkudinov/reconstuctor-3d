import { JSDOM } from 'jsdom'
import { Canvas, Image, ImageData } from 'canvas'

const globalAny: any = global
const dom = new JSDOM()
globalAny.document = dom.window.document
globalAny.Image = Image
globalAny.HTMLCanvasElement = Canvas
globalAny.ImageData = ImageData
globalAny.HTMLImageElement = Image

export const waitForCv = new Promise(resolve => {
  globalAny.Module = {
    onRuntimeInitialized: resolve
  }
  cv = require('../../../lib/opencv.js')
})
export let cv: any
