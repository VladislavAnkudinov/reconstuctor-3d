export default function JSONstringify(json: string): void {
  if (typeof json != 'string') {
    json = JSON.stringify(json, undefined, 2);
  }
  const
    arr = [],
    _string = 'color:green',
    _number = 'color:darkorange',
    _boolean = 'color:blue',
    _null = 'color:magenta',
    _key = 'color:red';

  json = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
    let style = _number;
    if (/^"/.test(match)) {
      if (/:$/.test(match)) {
        style = _key;
      } else {
        style = _string;
      }
    } else if (/true|false/.test(match)) {
      style = _boolean;
    } else if (/null/.test(match)) {
      style = _null;
    }
    arr.push(style);
    arr.push('');
    return '%c' + match + '%c';
  });

  arr.unshift(json);

  console.log.apply(console, arr);
}
