import * as math from 'mathjs'
import { SVD } from 'svd-js'
import * as mt from 'matlight'

export type SvdResult = {
  q: number[]
  u: number[][]
  v: number[][]
};

export function colReverse(m: number[][]): void {
  m.map((row: number[]) => row.reverse())
}

export function rowReverse(m: number[][]): void {
  m.reverse()
}

export function colSignToggle(m: number[][], col: number): void {
  m.forEach((row: number[]) => {
    row[col] = -row[col]
  })
}

export function rowSignToggle(m: number[][], i: number): void {
  m[i].forEach((elem: number, j: number) => {
    m[i][j] = -m[i][j]
  })
}

export function normSvd(svd: SvdResult): void {
  svd.v = math.transpose(svd.v)
  const qsort = [...svd.q]
  qsort.sort((b, a) => a > b ? 1 : a < b ? -1 : 0)
  const perm: number[] = qsort.map((val: number) => svd.q.findIndex(v => v === val))
  svd.q = qsort
  svd.v = perm.map((pos: number) => svd.v[pos])
  svd.u = math.transpose(svd.u)
  svd.u = perm.map((pos: number) => svd.u[pos])
  svd.u = math.transpose(svd.u)
  // svd.u[0].forEach((elem: number, j: number) => {
  //   if (elem >= 0) return
  //   colSignToggle(svd.u, j)
  //   rowSignToggle(svd.v, j)
  // })
}

export default function svd(m: number[][], alter = false): SvdResult {
  if (!alter) {
    const res: SvdResult = SVD(m)
    normSvd(res)
    return res
  } else {
    const res = mt.Matrix.SVD(new mt.Matrix(m))
    return {
      u: res.U.data,
      q: res.D.diagonalElements(),
      v: res.Vt.data
    }
  }
}
